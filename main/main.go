package main

import (
	api "adnaneModule/api"
	types "adnaneModule/types"
	"fmt"
	"math/rand"
)

func main() {
	rand.Seed(42)
	fmt.Println("Hello World")
	List_users := make([]types.UserAgent, 0)
	userChan := make(chan types.UserAgent)
	go api.StartAPI(List_users, userChan)
	go func() {
		for {
			<-userChan
		}
	}()
	select {}
}
