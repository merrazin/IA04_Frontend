package adnaneModule

// suivre la norme de nommage des struct
// *Response* pour les struct qui sont retournées par les fonctions
// *Request* pour les struct qui sont passées en paramètre des fonctions
// Majuscule pour les noms de struct

type Post struct {
	Id_post          string `json:"id_post"`
	Id_author        string `json:"id_user"`
	Content          string `json:"content"`
	Date_post        string
	Counter_likes    int
	List_id_likers   map[string]bool
	Counter_comments int
	List_comments    map[string]CommentData
	Counter_shares   int
	List_id_sharers  map[string]bool
	Opinion          float64
}

type CommentData struct {
	Id_comment   string
	Id_author    string
	Content      string
	Date_comment string
}

type UserAgent struct {
	Id_user         string  `json:"id_user"`
	Pseudo          string  `json:"pseudo"`
	Opinion         float64 `json:"opinion"` // 0.5 à la création
	PriorOpinion    float64 `json:"priorOpinion"`
	List_ownpost    map[string]bool
	List_sharedpost map[string]bool
	Feed            map[string]bool
	List_Followers  map[string]bool
	List_Following  map[string]bool
}

type New_user struct {
	Id_user      string  `json:"Id_user"`
	Pseudo       string  `json:"pseudo"`
	PriorOpinion float64 `json:"opinion"`
}

type Response_New_user struct {
	Id_user string `post:"id_user"` //post code 201
}

type New_post struct {
	Id_user string `json:"id_user"`
	Content string `json:"content"`
}

type Response_New_post struct {
	Id_post string `post:"id_post"` // si code 201
}

type New_comment struct {
	Id_post string `json:"id_post"`
	Id_user string `json:"id_user"`
	Content string `json:"content"`
}

type New_like struct {
	Id_user string `json:"id_user"`
	Id_post string `json:"id_post"`
}

type New_follow struct {
	Id_user          string `json:"id_user"`
	Id_user_followed string `json:"id_user_followed"`
}

type Request_sharepost struct {
	Id_user string `json:"id_user"`
	Id_post string `json:"id_post"`
}
type Request_getNewFollowingOriginalSharedPosts struct {
	Id_user string `json:"id_user"`
}
type Response_getNewFollowingOriginalPosts struct {
	Id_postsPublished []string `json:"id_postsPublished"` // les posts publiés par les utilisateurs suivis
}

type Response_getNewFollowingSharedPosts struct {
	Id_postsShared map[string][]string `json:"id_postsShared"` // l'utilisateur en clé et les posts partagés en valeur
}

type TrendPosts struct { // used for the trending posts in server.go (FOR PRIVATE USE ONLY)
	Id_post string
	Opinion float64
	Score   int
}

// suivre la norme de nommage des struct
// *Response* pour les struct qui sont retournées par les fonctions
// *Request* pour les struct qui sont passées en paramètre des fonctions
// Majuscule pour les noms de struct
