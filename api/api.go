package adnaneModule

import (
	types "adnaneModule/types"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net/http"
	"sort"
	"text/template"
	"time"
)

var I int = 0

// type HistogramData struct {
// 	HistrogramData  []float64
// }
type TopInfluencers struct {
	Count_Followers []int
	List_id         []string
	List_opinion    []float64
}

type SE_time struct {
	Se                   float64
	SeTest               float64
	TimeSTR              string
	HistrogramData       []float64
	TopInfluencersStruct TopInfluencers
}

func generateTopInfluencers(List_users []types.UserAgent) TopInfluencers {
	var topInfluencers TopInfluencers

	sort.Slice(List_users, func(i, j int) bool {
		return len(List_users[i].List_Followers) > len(List_users[j].List_Followers)
	})

	if len(List_users) > 10 {
		List_users = List_users[:10]
	}

	for _, user := range List_users {
		topInfluencers.List_id = append(topInfluencers.List_id, user.Id_user)
		topInfluencers.List_opinion = append(topInfluencers.List_opinion, user.Opinion)
		topInfluencers.Count_Followers = append(topInfluencers.Count_Followers, len(user.List_Followers))
	}

	return topInfluencers
}

func SE_time_users(List_users []types.UserAgent, timerNow time.Time) SE_time {
	var sum float64
	var Se SE_time
	for _, user := range List_users {
		sum += math.Abs(user.Opinion - user.PriorOpinion)
	}
	Se.TimeSTR = time.Since(timerNow).Round(time.Second).String()
	Se.Se = sum
	Se.HistrogramData = HistogramData(List_users)
	Se.TopInfluencersStruct = generateTopInfluencers(List_users)
	//Se.SeTest = sum + rand.Float64()
	return Se
}

func HistogramData(List_users []types.UserAgent) []float64 {
	var hist []float64
	for _, user := range List_users {
		hist = append(hist, user.Opinion)
	}
	return hist
}

func GenerateUser() types.UserAgent {

	// generate random followers
	mapFollowers := make(map[string]bool)
	for i := 0; i < rand.Intn(100); i++ {
		mapFollowers[fmt.Sprintf("user%d", i)] = true
	}
	I += 1
	userName := fmt.Sprintf("user%d", I)
	return types.UserAgent{Id_user: userName, Pseudo: "user1", Opinion: rand.Float64(), PriorOpinion: rand.Float64(), List_Followers: mapFollowers}

}

func appendUser(List_users []types.UserAgent) []types.UserAgent {
	return append(List_users, GenerateUser())
}

func StartAPI(List_users []types.UserAgent, userChan chan types.UserAgent) {
	// Create a time series for the plot
	timeNow := time.Now()
	http.HandleFunc("/displayData.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "api/displayData.js")
	})

	// Serve the HTML template
	http.HandleFunc("/display", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("api/index.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		tmpl.Execute(w, nil)
	})
	// HTTP handler for serving the SE value as JSON
	http.HandleFunc("/analytics", func(w http.ResponseWriter, r *http.Request) {
		msg, _ := json.Marshal(SE_time_users(List_users, timeNow))
		fmt.Fprintf(w, "%s", msg)
	})

	// Serve static files (HTML, JS, CSS, etc.) if needed
	//http.Handle("/", http.FileServer(http.Dir(".")))

	// Start the HTTP server in a goroutine
	go func() {
		log.Println("Listening on localhost:8080")
		log.Fatal(http.ListenAndServe(":8080", nil))
	}()

	// Regularly update the time series with data from the API
	go func() {
		for {
			time.Sleep(1000 * time.Millisecond) // Sleep for half a second
			newUser := GenerateUser()
			userChan <- newUser // Send the new user through the channel
			List_users = appendUser(List_users)
		}
	}()
}
