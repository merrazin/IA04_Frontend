// displayData.js
document.addEventListener("DOMContentLoaded", function () {
    // Connect to your Go server (assuming it's running on localhost:8080)
    const apiUrl = "http://localhost:8080/analytics";
    const ctx = document.getElementById("seChart").getContext("2d");
  
    // Initialize the SE chart
    const seChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: 'Squared Error over Time',
          borderColor: 'red',
          data: [],
        }],
      },
      options: {
        scales: {
          x: [{
            type: 'linear',
            position: 'bottom',
          }],
          y: [{
            type: 'linear',
            position: 'left',
          }],
        },
      },
    });
  
    // Initialize the histogram chart

    // Histogram Chart
    const histogramCtx = document.getElementById("histogramChart").getContext("2d");
    const histogramChart = new Chart(histogramCtx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Histogram',
                backgroundColor: 'blue',
                data: [],
            }],
        },
        options: {
            scales: {
                x: [{
                    type: 'linear',
                    position: 'bottom',
                }],
                y: [{
                    type: 'linear',
                    position: 'left',
                }],
            },
        },
    });

    const TopFollowersCtx = document.getElementById("barChart").getContext("2d");
    const TopFollowersChart = new Chart(TopFollowersCtx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Followers Count',
                data: [],
                backgroundColor: [], // Added line for colors
                borderWidth: 2, // Border width for each bar
            }],
        },
        options: {
            indexAxis: 'y', // Specify the horizontal axis
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Top Influencers Followers Count',
                },
            },
            scales: {
                x: {
                    beginAtZero: true,
                },
            },
        },
    });
    

    // Function to fetch data from the API and update the chart
    async function fetchData() {
      try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        const time = data.TimeSTR;
        updateChartSEPLOT(data.Se,time);
        updateHistogramChart(data.HistrogramData);
        updateTopFollowersChart(data.TopInfluencersStruct)
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  
    // Function to update the chart data
    function updateChartSEPLOT(seValue, time) {

      seChart.data.labels.push(time);
      seChart.data.datasets[0].data.push(seValue);  
      // Update the chart
      // Remove the oldest data point if the chart exceeds a certain number of points
       const maxDataPoints = 200;
       if (seChart.data.labels.length > maxDataPoints) {
         seChart.data.labels.shift();
       seChart.data.datasets[0].data.shift();
       }
      seChart.update();
    }

    function updateHistogramChart(histogramData) {

      histogramChart.data.labels = [];
      histogramChart.data.datasets[0].data = [];
      histogramChart.data.datasets[0].backgroundColor = []; // Added line for colors

      // Number of bins(les barres) for the histogram
      const numBins = 10;
      const binWidth = 1 / numBins;

      // Initialize bins
      const bins = Array.from({ length: numBins }, (_, index) => index * binWidth);

      // Count values in each bin
      const counts = Array(numBins).fill(0);
      for (const value of histogramData) {
          const binIndex = Math.floor(value / binWidth);
          counts[binIndex]++;
          
      }

      // Update chart data
      for (let i = 0; i < numBins; i++) {
          const binLabel = `${(i * binWidth).toFixed(1)}-${((i + 1) * binWidth).toFixed(1)}`;
          histogramChart.data.labels.push(binLabel);
          histogramChart.data.datasets[0].data.push(counts[i]); 

          
          const normalizedX = i / (numBins - 1); // Normalize to [0, 1]
          const color = `rgba(${Math.round(255 * (normalizedX))}, 0, ${Math.round(255 * (1-normalizedX))}, 0.8)`;
          histogramChart.data.datasets[0].backgroundColor.push(color);
      }

      histogramChart.update();
  }
  function updateTopFollowersChart(topInfluencers) {
    // Clear previous data
    TopFollowersChart.data.labels = [];
    TopFollowersChart.data.datasets[0].data = [];
    TopFollowersChart.data.datasets[0].backgroundColor = []; // Added line for colors

    // Update chart data
    for (let i = 0; i < topInfluencers.List_id.length; i++) {
       const nameAndOpinion = `${topInfluencers.List_id[i]} (${topInfluencers.List_opinion[i].toFixed(2)})`;
        TopFollowersChart.data.labels.push(nameAndOpinion);
        TopFollowersChart.data.datasets[0].data.push(topInfluencers.Count_Followers[i]);

        // Choose color based on opinion
        const opinion = topInfluencers.List_opinion[i];
        const color = `rgba(${Math.round(255 * (1 - opinion))}, 0, ${Math.round(255 * opinion)}, 0.8)`;
        TopFollowersChart.data.datasets[0].backgroundColor.push(color);
    }

    TopFollowersChart.update();
}

    // Fetch data periodically
    setInterval(fetchData, 1005); // Adjust the interval as needed
  });
  