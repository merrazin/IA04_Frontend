// package main

// import (
// 	"fmt"
// 	"os"

// 	"gonum.org/v1/gonum/graph/encoding/dot"
// 	"gonum.org/v1/gonum/graph/simple"
// )

// func main() {
// 	g := simple.NewDirectedGraph()

// 	// Add nodes for users
// 	user1 := g.NewNode()
// 	user2 := g.NewNode()

// 	g.AddNode(user1)
// 	g.AddNode(user2)

// 	// Add an edge to represent a relationship (e.g., following)
// 	g.SetEdge(simple.Edge{F: user1, T: user2})

// 	// Use the DotEncoder to generate a DOT format representation
// 	data, err := dot.Marshal(g, "socialGraph", "", "\t", false)
// 	if err != nil {
// 		fmt.Println("Error:", err)
// 		return
// 	}

// 	// Write the DOT data to a file (or use it as needed)
// 	file, err := os.Create("socialGraph.dot")
// 	if err != nil {
// 		fmt.Println("Error:", err)
// 		return
// 	}
// 	defer file.Close()
// 	file.Write(data)

// 	fmt.Println("Graph data written to socialGraph.dot")
// }

package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Relationship struct {
	Rid  int
	Type string
	With int
}

type Person struct {
	Pid      int
	Name     string
	Relation map[string][]int
}

//-------------------------------------------------------------
func (p *Person) AddRelation(q *Person, name string) *Person {
	r := &Relationship{101, name, q.Pid}

	if p.Relation == nil {
		p.Relation = make(map[string][]int)
	}
	//Initializing map

	p.Relation[name] = append(p.Relation[name], r.Rid) //to relatinship id only
	return p
}

//-------------------------------------------------------------

func main() {
	//api request to be match with url SOMETHING LIKE CHAT INTERFACE
	http.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Add("Content-type", "application/json")

		p1 := &Person{Pid: 1}
		p1.Name = "P" //this id and other details may come from pages: our and other
		p2 := &Person{2, "S", nil}

		p1.AddRelation(p2, "WIFE")
		p1.AddRelation(p2, "FRIEND")
		p2.AddRelation(p1, "HUSBAND")

		b, err := json.Marshal(p1)
		if err != nil {
			log.Fatal(err)
		}
		rw.Write(b)
	})
	log.Println("Running on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
