package main

// import (
// 	"html/template"
// 	"net/http"

// 	"github.com/gin-gonic/gin"
// )

// // User represents a user in the social network.
// type User struct {
// 	ID        int
// 	Username  string
// 	Profile   string
// 	Political float64
// }

// var users = []User{
// 	{1, "user1", "Profile 1", 0.3},
// 	{2, "user2", "Profile 2", 0.7},
// 	// Add more users as needed
// }

// func main() {
// 	router := gin.Default()

// 	// Serve static files (e.g., user profile images)
// 	router.Static("/static", "./static")

// 	// Initialize the HTML template rendering engine
// 	htmlTemplate := template.Must(template.New("index").Parse(`
// 	<!DOCTYPE html>
// 	<html lang="en">
// 	<head>
// 		<meta charset="UTF-8">
// 		<meta http-equiv="X-UA-Compatible" content="IE=edge">
// 		<meta name="viewport" content="width=device-width, initial-scale=1.0">
// 		<title>Social Network Graph</title>
// 		<!-- Add your CSS styles for nodes here -->

// 		<style>
// 			/* Add your CSS styles for nodes here */
// 			.node {
// 				width: 50px;
// 				height: 50px;
// 				background-color: #3498db;
// 				color: #fff;
// 				text-align: center;
// 				line-height: 50px;
// 				border-radius: 50%;
// 				position: absolute;
// 				cursor: pointer;
// 			}
// 		</style>
// 	</head>
// 	<body>
// 		<div id="graph-container">
// 			<!-- The social network graph will be rendered here -->
// 			<div class="node" style="left: 50px; top: 50px;" onmousedown="startDragging(event, this)">User 1</div>
// 			<div class="node" style="left: 150px; top: 100px;" onmousedown="startDragging(event, this)">User 2</div>
// 		</div>

// 		<!-- You can include additional JavaScript here if needed -->
// 		<script>
// 			let isDragging = false;
// 			let offset = { x: 0, y: 0 };

// 			function startDragging(e, element) {
// 				isDragging = true;

// 				// Get the initial mouse position
// 				const rect = element.getBoundingClientRect();
// 				offset = {
// 					x: e.clientX - rect.left,
// 					y: e.clientY - rect.top
// 				};

// 				// Attach event listeners
// 				document.addEventListener('mousemove', (event) => handleDragging(event, element));
// 				document.addEventListener('mouseup', stopDragging);
// 			}

// 			function handleDragging(e, element) {
// 				if (!isDragging) return;

// 				// Update the element's position
// 				element.style.left = e.clientX - offset.x + 'px';
// 				element.style.top = e.clientY - offset.y + 'px';
// 			}

// 			function stopDragging() {
// 				isDragging = false;

// 				// Remove event listeners
// 				document.removeEventListener('mousemove', handleDragging);
// 				document.removeEventListener('mouseup', stopDragging);
// 			}
// 		</script>
// 	</body>
// 	</html>

// 	`))

// 	router.SetHTMLTemplate(htmlTemplate)

// 	// Define a route to render the social network graph
// 	router.GET("/", func(c *gin.Context) {
// 		c.HTML(http.StatusOK, "index", nil)
// 	})

// 	// API endpoint to get user data
// 	router.GET("/api/users", func(c *gin.Context) {
// 		c.JSON(http.StatusOK, users)
// 	})

// 	// Run the server
// 	router.Run(":8080")
// }
